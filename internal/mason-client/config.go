package mason_client

import (
	"os"
)

type Config struct {
	APIKey    string
	Endpoints Endpoints
}

func GetConfig(providedApiKey string) (Config, error) {
	apiKey := os.Getenv("MASON_API_KEY")
	if providedApiKey != "" {
		apiKey = providedApiKey
	}

	domain := os.Getenv("MASON_DOMAIN")
	platformURL, err := buildPlatformUrl(domain)
	if err != nil {
		return Config{}, err
	}
	websocketURL, err := buildWebsocketUrl(domain)
	if err != nil {
		return Config{}, err
	}
	endpoints := map[EnvironmentType]Endpoint{
		Platform:  Endpoint(platformURL),
		WebSocket: Endpoint(websocketURL),
	}

	return Config{
		APIKey:    apiKey,
		Endpoints: endpoints,
	}, nil
}
