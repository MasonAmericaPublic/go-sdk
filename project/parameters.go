package project

// CreateProjectOpts stores parameters used to create a Project
type CreateProjectOpts struct {
	// API Level is an integer value that uniquely identifies the framework API
	// revision offered by a version of the Android platform
	APILevel *APILevel `json:"apiLevel"`
	// Description is used to represent how to describe a Project.
	Description *string `json:"description"`
	// DeviceFamily corresponds to the device type D450, G450, and so on.
	DeviceFamily *DeviceFamily `json:"deviceFamily"`
	// Name is the unique human readable Project identifier
	Name *string `json:"name"`
}
