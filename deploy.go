package mason_go_sdk

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/MasonAmericaPublic/go-sdk.git/deploy"
	mason_client "gitlab.com/MasonAmericaPublic/go-sdk.git/internal/mason-client"
)

const (
	deploymentsPath path = "v1/default/deployments"
)

// Deploy will deploy the specified artifact to the given group
func (m *Mason) Deploy(ctx context.Context, params deploy.DeployParameters) (*deploy.Deployment, error) {
	jsonBody, err := json.Marshal(params)
	if err != nil {
		return nil, err
	}

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   string(deploymentsPath),
		body:   jsonBody,
		method: http.MethodPost,
	})
	if err != nil {
		return nil, err
	}

	errorResponse := ErrorResponse{}
	result := deploy.Deployment{}
	err = m.client.Do(req, &result, &errorResponse)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return nil, fmt.Errorf("%s, error: %s, message: %s", err.Error(), errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}
	return &result, nil
}
