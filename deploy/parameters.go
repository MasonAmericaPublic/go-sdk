package deploy

type DeployParameters struct {
	Name           string         `json:"name"`
	GroupId        string         `json:"groupId"`
	Version        string         `json:"version"`
	Type           DeploymentType `json:"type"`
	Push           bool           `json:"push,omitempty"`
	DeployInsecure bool           `json:"deployInsecure,omitempty"`
}
