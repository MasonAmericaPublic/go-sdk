package mason_go_sdk

import (
	"bytes"
	"context"
	"fmt"
	"gitlab.com/MasonAmericaPublic/go-sdk.git/artifacts"
	mason_client "gitlab.com/MasonAmericaPublic/go-sdk.git/internal/mason-client"
	"mime/multipart"
	"net/http"
	"net/url"
)

type path string

const (
	artifactsPath path = "v1/default/artifacts/upload"
)

type MasonArtifact struct {
	data         []byte
	fileName     string
	artifactType artifacts.ArtifactType
}

// RegisterSplash will upload a bootanimation to the Platform.
func (m *Mason) RegisterSplash(ctx context.Context, fileName string, data []byte) (*artifacts.Artifact, error) {
	return m.registerArtifact(ctx, &MasonArtifact{artifactType: artifacts.SplashType, fileName: fileName, data: data})
}

// RegisterBootanimation will upload a bootanimation to the Platform.
func (m *Mason) RegisterBootanimation(ctx context.Context, fileName string, data []byte) (*artifacts.Artifact, error) {
	return m.registerArtifact(ctx, &MasonArtifact{artifactType: artifacts.BootAnimationType, fileName: fileName, data: data})
}

// RegisterAPK will upload an APK to the Platform.
func (m *Mason) RegisterAPK(ctx context.Context, fileName string, data []byte) (*artifacts.Artifact, error) {
	return m.registerArtifact(ctx, &MasonArtifact{artifactType: artifacts.APKType, fileName: fileName, data: data})
}

// RegisterConfig will upload a Mason OS config to the Platform.
func (m *Mason) RegisterConfig(ctx context.Context, fileName string, data []byte) (*artifacts.Artifact, error) {
	return m.registerArtifact(ctx, &MasonArtifact{artifactType: artifacts.ConfigType, fileName: fileName, data: data})
}

// RegisterArtifact will upload an artifact to the Platform.
func (m *Mason) registerArtifact(ctx context.Context, artifact *MasonArtifact) (*artifacts.Artifact, error) {
	v := url.Values{}
	v.Add("type", string(artifact.artifactType))

	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("artifact", artifact.fileName)
	if err != nil {
		return nil, err
	}

	_, err = part.Write(artifact.data)
	if err != nil {
		return nil, err
	}

	_ = writer.WriteField("artifact", artifact.fileName)
	err = writer.Close()
	if err != nil {
		return nil, err
	}

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   string(artifactsPath),
		method: http.MethodPost,
		body:   body.Bytes(),
		params: v,
	})
	if err != nil {
		return nil, err
	}
	req.Header.Add("Content-Type", writer.FormDataContentType())

	result := artifacts.Artifact{}
	errorResponse := ErrorResponse{}
	err = m.client.Do(req, &result, &errorResponse)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return nil, fmt.Errorf("%s, error: %s, message: %s", err.Error(), errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}
	return &result, nil
}
