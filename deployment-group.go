package mason_go_sdk

import (
	"context"
	"fmt"
	mason_client "gitlab.com/MasonAmericaPublic/go-sdk.git/internal/mason-client"
	"gitlab.com/MasonAmericaPublic/go-sdk.git/project"
	"net/http"
	"time"
)

const (
	groupPath path = "v1/default/deployment-groups"
)

// A DeploymentGroup is a group of devices configured to run specific artifacts through deployments.
type DeploymentGroup struct {
	ID           string                `json:"id"`
	Name         string                `json:"name"`
	Description  string                `json:"description"`
	CreatedAt    time.Time             `json:"createdAt"`
	UpdatedAt    time.Time             `json:"updatedAt"`
	APILevel     *project.APILevel     `json:"apiLevel,omitempty"`
	DeviceFamily *project.DeviceFamily `json:"deviceFamily,omitempty"`
}

type deploymentGroupsResponse struct {
	DeploymentGroups []DeploymentGroup `json:"data"`
}

// GetDeploymentGroupByName retrieves the deployment group with the given name.
func (m *Mason) GetDeploymentGroupByName(ctx context.Context, name string) (*DeploymentGroup, error) {
	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   string(groupPath),
		method: http.MethodGet,
		params: map[string][]string{
			"name": {name},
		},
	})
	if err != nil {
		return nil, err
	}

	var groups deploymentGroupsResponse
	var errResponse ErrorResponse
	_, err = m.client.DoRequest(req, &groups, &errResponse, true)
	if err != nil {
		if errResponse.Context != "" {
			return nil, fmt.Errorf("%s, error: %s, message: %s", err.Error(), errResponse.Type, errResponse.Context)
		}

		return nil, err
	}

	if len(groups.DeploymentGroups) == 0 {
		return nil, fmt.Errorf("deployment group not found")
	}

	return &groups.DeploymentGroups[0], nil
}
