package mason_go_sdk

type ErrorResponse struct {
	Type    string `json:"type"`
	Context string `json:"context"`
}
