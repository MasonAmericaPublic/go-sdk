package mason_go_sdk

import (
	mason_client "gitlab.com/MasonAmericaPublic/go-sdk.git/internal/mason-client"
)

// Mason contains integrations for the Platform. It has the ability to register artifacts, deploy APKs, etc.
type Mason struct {
	client *mason_client.MasonHttpClient
}

// NewMason creates a new instance of the Mason client. This constructor will read your
// API key from the MASON_API_KEY environment variable.
func NewMason(mason *Mason, apiKey string) error {
	cfg, err := mason_client.GetConfig(apiKey)
	if err != nil {
		return err
	}
	client, err := mason_client.NewMasonClient(cfg)
	if err != nil {
		return err
	}

	mason.client = client
	return nil
}

func (m *Mason) AddUserAgent(product string, version string, comment *string) {
	m.client.AddUserAgent(product, version, comment)
}
