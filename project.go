package mason_go_sdk

import (
	"context"
	"encoding/json"
	"fmt"
	mason_client "gitlab.com/MasonAmericaPublic/go-sdk.git/internal/mason-client"
	"gitlab.com/MasonAmericaPublic/go-sdk.git/project"
	"net/http"
	"net/url"
)

const (
	projectPath string = "v1/default/projects"
)

type projectsResponse struct {
	Projects []project.Project `json:"data"`
}

// CreateProject validates the given parameters and creates a project using them.
func (m *Mason) CreateProject(
	ctx context.Context,
	opts project.CreateProjectOpts) (*project.Project, error) {
	jsonBody, err := json.Marshal(opts)
	if err != nil {
		return nil, err
	}

	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   projectPath,
		method: http.MethodPost,
		body:   jsonBody,
	})
	if err != nil {
		return nil, err
	}

	result := project.Project{}
	errorResponse := ErrorResponse{}
	err = m.client.Do(req, &result, &errorResponse)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return nil, fmt.Errorf("request failed with error: %s, message: %s", errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}
	return &result, nil
}

// GetProject retrieves the project with the given projectName
func (m *Mason) GetProject(ctx context.Context, projectName string) (*project.Project, error) {
	req, err := m.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   projectPath,
		method: http.MethodGet,
		params: url.Values{
			"name": []string{projectName},
		},
	})
	if err != nil {
		return nil, err
	}

	result := projectsResponse{}
	errorResponse := ErrorResponse{}
	err = m.client.Do(req, &result, &errorResponse)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return nil, fmt.Errorf("%s, error: %s, message: %s", err.Error(), errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}

	if len(result.Projects) == 0 {
		return nil, fmt.Errorf("project not found")
	}

	return &result.Projects[0], nil
}
