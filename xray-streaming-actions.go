package mason_go_sdk

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"net/url"
	p "path"
	"strings"
	"time"

	mason_client "gitlab.com/MasonAmericaPublic/go-sdk.git/internal/mason-client"

	"github.com/gorilla/websocket"
	"github.com/pion/webrtc/v3"
	log "github.com/sirupsen/logrus"
)

const (
	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second
	connHost = "localhost"
	connType = "tcp"
)

type ICEAddressResponse struct {
	ICEServerList []ICEServer `json:"ice_servers,omitempty"`
}

type ICEServer struct {
	Username   string `json:"username,omitempty"`
	Credential string `json:"credential,omitempty"`
	URL        string `json:"url,omitempty"`
}

type RTCAnswer struct {
	Type webrtc.SDPType
	SDP  string
}

type WriteRequest struct {
	MessageType int
	Data        []byte
}

type PeerWriter chan WriteRequest

func CreateRemoteConnectionWriter(conn *websocket.Conn, writer PeerWriter, errc chan<- error) {
	go func() {
		for writeRequest := range writer {
			err := conn.WriteMessage(writeRequest.MessageType, writeRequest.Data)
			if err != nil {
				errc <- err
			}
		}
	}()
}

func CreateLocalConnectionWriter(conn net.Conn, writer PeerWriter, errc chan<- error) {
	go func() {
		for writeRequest := range writer {
			_, err := conn.Write(writeRequest.Data)
			if err != nil {
				errc <- err
			}
		}
	}()
}

func CreateLocalOutputWriter(writer PeerWriter) {
	go func() {
		for writeRequest := range writer {
			fmt.Println(string(writeRequest.Data))
		}
	}()
}

type StreamingXrayClient struct {
	mason           *Mason
	deviceID        string
	command         string
	apiKey          string
	c               net.Conn
	remoteWriter    PeerWriter
	localWriter     PeerWriter
	localConnWriter PeerWriter
	peerConnection  *webrtc.PeerConnection
	dataChannel     *webrtc.DataChannel
	urlPath         string
	connPort        string
}

func (m *Mason) NewStreamingXrayClient(ctx context.Context, deviceID, cmd string, query url.Values) (*StreamingXrayClient, error) {
	ep := fmt.Sprintf("%s/%s", string(m.client.GetBasePath(mason_client.WebSocket)), p.Join(xrayPath, deviceID, cmd))
	if query.Encode() != "" {
		ep = fmt.Sprintf("%s?%s", ep, query.Encode())
	}
	return &StreamingXrayClient{
		mason:        m,
		deviceID:     deviceID,
		apiKey:       m.client.APIKey,
		remoteWriter: make(PeerWriter),
		localWriter:  make(PeerWriter),
		command:      cmd,
		urlPath:      ep,
	}, nil
}

func (m *Mason) NewLocalProxyStreamingXrayClient(ctx context.Context, connport, deviceID, cmd string) (*StreamingXrayClient, error) {
	x, err := m.NewStreamingXrayClient(ctx, deviceID, cmd, url.Values{})
	if err != nil {
		return nil, err
	}
	x.connPort = connport
	return x, nil
}

func (x *StreamingXrayClient) Close() {
	if x.peerConnection != nil {
		x.peerConnection.Close()
	}
	if x.c != nil {
		x.c.Close()
	}
	close(x.remoteWriter)
	close(x.localWriter)
	if x.localConnWriter != nil {
		close(x.localConnWriter)
	}
}

func (x *StreamingXrayClient) HandleClientWebRTC(ctx context.Context) error {
	servers, err := x.getICEServers(ctx)
	if err != nil {
		return err
	}

	config := webrtc.Configuration{
		ICEServers: servers,
	}
	peerConnection, err := webrtc.NewPeerConnection(config)
	if err != nil {
		return err
	}
	x.peerConnection = peerConnection

	dataChannel, err := peerConnection.CreateDataChannel("data", nil)
	if err != nil {
		return err
	}
	x.dataChannel = dataChannel
	socket, err := x.getRemoteWebsocketConn()
	if err != nil {
		return err
	}

	errc := make(chan error, 1)
	defer close(errc)
	CreateRemoteConnectionWriter(socket, x.remoteWriter, errc)
	CreateLocalOutputWriter(x.localWriter)

	completionGate := make(chan bool, 1)
	x.ReadSignalingMessages(ctx, socket, completionGate, errc)

	select {
	case err := <-errc:
		x.Close()
		return err
	case <-ctx.Done():
		x.Close()
		return nil
	}
}

func (x *StreamingXrayClient) ReadSignalingMessages(ctx context.Context, conn *websocket.Conn, completionGate chan bool, errc chan<- error) {
	type readMessage struct {
		messageType int
		message     []byte
		err         error
	}
	messages := make(chan readMessage)
	defer close(messages)
	go func() {
		for {
			msgType, msg, err := conn.ReadMessage()
			if err != nil {
				code := websocket.CloseNormalClosure
				if e, ok := err.(*websocket.CloseError); ok {
					if e.Code != websocket.CloseNoStatusReceived {
						code = e.Code
					}
				}
				if code != websocket.CloseNormalClosure {
					errc <- err
				}
				completionGate <- true
				log.Debug("read message error: ", err.Error(), " msgtype: ", msgType, " code: ", code)
				return
			}
			select {
			case <-completionGate:
				return
			case <-ctx.Done():
				return
			default:
				messages <- readMessage{
					messageType: msgType,
					message:     msg,
					err:         err,
				}
			}
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return
		case <-completionGate:
			log.Debug("done initiating webrtc connection")
			return
		case message := <-messages:
			if message.messageType == websocket.TextMessage {
				var answer RTCAnswer

				if !strings.Contains(string(message.message), "device:") {
					answerErr := json.Unmarshal(message.message, &answer)

					if answerErr == nil && x.peerConnection.RemoteDescription() == nil {
						log.Debug("received remote answer")
						remoteDesc := webrtc.SessionDescription{
							Type: webrtc.SDPTypeAnswer,
							SDP:  answer.SDP,
						}
						err := x.peerConnection.SetRemoteDescription(remoteDesc)
						if err != nil {
							log.Error(err)
						}
					}

					var remoteCandidate webrtc.ICECandidateInit
					candidateErr := json.Unmarshal(message.message, &remoteCandidate)
					if candidateErr != nil {
						log.Error(candidateErr.Error())
						continue
					}

					log.Debug("received remote ice candidate")
					err := x.peerConnection.AddICECandidate(remoteCandidate)
					if err != nil {
						log.Error(err)
					}
				}

				// Once we are connected to the device, we can inititate the webrtc connection
				if string(message.message) == "device:ok" {
					x.brokerWebRTC(conn, completionGate, errc, x.command)
				}
			}
		}
	}
}

func (x *StreamingXrayClient) brokerWebRTC(conn *websocket.Conn, completionGate chan<- bool, errc chan<- error, cmd string) {
	x.peerConnection.OnConnectionStateChange(func(state webrtc.PeerConnectionState) {
		log.Debug("peer connection state changed to: ", state.String())
		if state == webrtc.PeerConnectionStateFailed || state == webrtc.PeerConnectionStateDisconnected || state == webrtc.PeerConnectionStateClosed {
			errc <- fmt.Errorf("peer connection closed")
		}
	})

	x.peerConnection.OnICECandidate(func(i *webrtc.ICECandidate) {
		if i != nil {
			log.Debug("sending local ice candidate to remote peer")
			jsonData, err := json.Marshal(i.ToJSON())
			if err != nil {
				errc <- err
			}
			x.remoteWriter <- WriteRequest{
				MessageType: websocket.TextMessage,
				Data:        jsonData,
			}
		}
	})

	x.dataChannel.OnOpen(func() {
		log.Debug("data channel open")
		completionGate <- true
		if x.connPort != "" {
			x.openLocalTCPSocket(cmd, errc)
			go func() {
				x.scanLocalTCPSocket(errc)
			}()
		} else {
			go func() {
				select {} // wait until cancelled.
			}()
		}
		defer conn.Close()
		log.Debug("closing websocket")
	})

	x.dataChannel.OnMessage(func(msg webrtc.DataChannelMessage) {
		// If we have a local connection, write the remote data there
		// Otherwise output it to the console as a String.
		if x.localConnWriter != nil {
			x.localConnWriter <- WriteRequest{Data: msg.Data}
		} else {
			x.localWriter <- WriteRequest{Data: msg.Data}
		}
	})

	log.Debug("creating peer offer")

	offer, err := x.peerConnection.CreateOffer(nil)
	if err != nil {
		errc <- err
		return
	}

	log.Debug("setting local description")
	err = x.peerConnection.SetLocalDescription(offer)
	if err != nil {
		errc <- err
		return
	}

	log.Debug("sending offer to remote peer", offer.SDP)
	jsonData, err := json.Marshal(offer)
	if err != nil {
		errc <- err
	}
	x.remoteWriter <- WriteRequest{
		MessageType: websocket.TextMessage,
		Data:        jsonData,
	}
}

func (x *StreamingXrayClient) scanLocalTCPSocket(errc chan<- error) {
	log.Debug("reading local port")
	buff := make([]byte, 1024*100)
	for {
		bytes, err := x.c.Read(buff)
		if err != nil {
			errc <- err
			return
		}
		if bytes > 0 && x.dataChannel.ReadyState() == webrtc.DataChannelStateOpen {
			err := x.dataChannel.Send(buff[0:bytes])
			if err != nil {
				errc <- err
				return
			}
		}
	}
}

func (x *StreamingXrayClient) openLocalTCPSocket(cmd string, errc chan<- error) {
	ln, err := net.Listen(connType, connHost+":"+x.connPort)
	if err != nil {
		errc <- err
	}

	fmt.Printf("Xray is connected. Waiting for local %s client connection on port %s \n", cmd, x.connPort)
	if cmd == "adb" {
		fmt.Printf("run: adb connect localhost:%s \n", x.connPort)
	}

	tcpConn, err := ln.Accept()
	if err != nil {
		errc <- err
	}

	if cmd == "adb" {
		fmt.Println("Successfully connected adb. All adb commands will now be forwarded to your device")
	}

	x.c = tcpConn
	x.localConnWriter = make(PeerWriter)
	CreateLocalConnectionWriter(tcpConn, x.localConnWriter, errc)
}

func (x StreamingXrayClient) getRemoteWebsocketConn() (*websocket.Conn, error) {
	// Add specific Headers to pass along
	sendHeaders := &http.Header{}
	sendHeaders.Add("Sec-WebSocket-Protocol", x.apiKey)
	sendHeaders.Add("Authorization", fmt.Sprintf("BASIC %s", x.apiKey))
	socket, resp, err := websocket.DefaultDialer.Dial(x.urlPath, *sendHeaders)
	if err != nil {
		if resp != nil {
			errorResponse := ErrorResponse{}
			decoder := json.NewDecoder(resp.Body)
			decodeErr := decoder.Decode(&errorResponse)
			if decodeErr != nil {
				log.Error("Could not decode error response: ", decodeErr)
			}
			return nil, fmt.Errorf("%s, statusCode: %d, error: %s, message: %s", err.Error(), resp.StatusCode, errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}

	err = socket.SetReadDeadline(time.Now().Add(pongWait))
	if err != nil {
		return nil, err
	}
	socket.SetPingHandler(func(string) error {
		err = socket.SetReadDeadline(time.Now().Add(pongWait))
		if err != nil {
			return err
		}
		x.remoteWriter <- WriteRequest{MessageType: websocket.PongMessage, Data: []byte{}}
		// Errors on writing the ping response are sent to the error channel
		return nil
	})
	return socket, nil
}

func (x StreamingXrayClient) getICEServers(ctx context.Context) ([]webrtc.ICEServer, error) {
	ep := p.Join(xrayPath, "ice-servers")

	req, err := x.mason.generateHttpRequest(ctx, httpRequestConfig{
		et:     mason_client.Platform,
		path:   ep,
		method: http.MethodGet,
	})
	if err != nil {
		return nil, err
	}
	result := ICEAddressResponse{}
	errorResponse := ErrorResponse{}
	err = x.mason.client.Do(req, &result, &errorResponse)
	if err != nil {
		// if we have an error response use it
		if errorResponse.Context != "" {
			return nil, fmt.Errorf("request failed with error: %s, message: %s", errorResponse.Type, errorResponse.Context)
		}
		return nil, err
	}

	if len(result.ICEServerList) < 1 {
		return nil, fmt.Errorf("could not find any ice servers")
	}

	var servers []webrtc.ICEServer
	for _, server := range result.ICEServerList {
		ice := webrtc.ICEServer{
			URLs: []string{server.URL},
		}
		if server.Username != "" {
			ice.Username = server.Username
		}
		if server.Credential != "" {
			ice.CredentialType = webrtc.ICECredentialTypePassword
			ice.Credential = server.Credential
		}
		servers = append(servers, ice)
	}
	return servers, nil
}
